

package skyview.geometry.projecter.toast;

import skyview.geometry.Util;
import skyview.geometry.projecter.Toa;

/**
 * Print out the vertices of the pixels in a TOAST projection.
 * @author tmcglynn
 */
public class ToaPixels {
    
    public static void main(String[] args) {
        int level = 0;
        if (args.length > 0) {
            level = Integer.parseInt(args[0]);
        } else {
            System.err.println("Usage: ToaPixels level [even odd]");
            System.exit(-1);
        }
        String even = "";
        String odd = "";
        if (args.length > 2) {
            even = args[1];
            odd  = args[2];
        }
        
        int n = (int) Math.pow(2,level);
        Toa t = new Toa();
        double[][][] values = t.tile(0, 0, 0, level);
        int ny = values.length;
        int nx = values[0].length;
        boolean isEven = true;
        
        for (int i=0; i<ny-1; i += 1) {
            int ip = i+1;
            for (int j=0; j<nx-1; j += 1) {
                int jp = j+1;
                double[] p00 = values[i][j];
                double[] p01 = values[i][jp];
                double[] p10 = values[ip][j];
                double[] p11 = values[ip][jp];
                
                // Print out the pixel value as vertices
                String suffix = odd;
                if (isEven) {
                    suffix = even;
                }
                printPixel(p00, suffix);
                printPixel(p01, suffix);
                printPixel(p11, suffix);
                printPixel(p10, suffix);
                isEven = ! isEven;
            }
            isEven = ! isEven;
        }
    }
    
    static void printPixel(double[] c, String suffix) {
        System.out.printf("%12.8f %12.8f %12.8f %s\n", c[0], c[1], c[2], suffix);
    }
}
