package skyview.survey;

/** This class is used for exceptions problems with
 *  the data in a survey.
 */

public class SurveyException extends Exception {
    
    public SurveyException() {
    }
    
    public SurveyException(String cause) {
	super(cause);
    }
    
    public SurveyException(String cause, Exception prior) {
        super(cause, prior);
    }
}
    
    
