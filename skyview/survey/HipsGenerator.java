package skyview.survey;

import skyview.executive.Settings;
import skyview.survey.Image;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;

import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

/** This class simply returns the directory in which a HiPS is stored. */
public class HipsGenerator implements ImageGenerator {
    
    
    /** Get a HiPS request.  The information we need is the
     *  directory in which the data resides and the scale of the image.
     */
    public void getImages(double ra, double dec, double size, java.util.ArrayList<String> spells)  {
        String spell = Settings.get("HipsDir");
        String scale = Settings.get("Scale");
        double dscl = 0.00027777777;   // 1" in degrees
        if (scale != null) {
            scale = scale.trim();
            String[] fields = scale.split(" ");
            if (fields.length == 1) {
                try {
                    dscl = Math.abs(Double.parseDouble(scale));
                } catch (Exception e) {
                    System.err.println("Error parsing scale for HiPS");
                    // Just ignore
                }
            } else if (fields.length == 2) {
                try {
                    double x1 = Math.abs(Double.parseDouble(fields[0]));
                    double x2 = Math.abs(Double.parseDouble(fields[1]));
                    double mn = Math.min(x1,x2);
                    if (mn > 0) {
                        dscl = mn;
                    }
                } catch (Exception e) {
                    System.err.println("Error parsing scales in HiPS");
                    // Ignore and continue with default.
                }
            }
        }
        spell += "|" + dscl;  
        spells.add(spell);
    }
}
