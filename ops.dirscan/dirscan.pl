#!/usr/bin/perl -wT
#--- Scans SkyView cache, deletes old files after recording query statistics.  
#--- A report is sent  via email.

use strict; 

use FindBin;
# This fragment should be included to enable FindBin
# to be used in Taint mode.  It detaints the current
# directory available in FindBin.  It must be
# included immediately after 'use FindBin' and before
# $FindBin::Bin is invoked.
#
# Later versions of Perl do something about this automatically
# but not in the version installed at the HEASARC.
BEGIN {
    $FindBin::Bin =~ /(.*)/;
    $FindBin::Bin = $1;
}
my $libPath = $FindBin::Bin . $INC{"SkyView_Env.pm"};
my $currentDir =$FindBin::Bin;

use lib "$FindBin::Bin/../../cgi/cgifiles/lib";
use SkyView_Env;

my $debug=1;  # sends output via mail if run as cron job

my $tempspaceDir = $ENV{TMP_SHARED};
$tempspaceDir = checkVar($tempspaceDir); 
if (! $tempspaceDir) {
   print "*** Error: Cannot find temporary space directory \n\n";
   exit();
}

my $statDir = "/web_chroot/data/logs/account"; #--temp ??
my $statFile = "querystats"; 

my $skvjar =$ENV{JARDIR};
$skvjar = checkVar($skvjar); 

my $timeInCache = $ENV{TMP_TIME};
$timeInCache = checkVar($timeInCache);

my $largeFileSize = $ENV{TMP_LARGE};
$largeFileSize = checkVar($largeFileSize);

my $largeFileTimeInCache = $ENV{TMP_LARGE_TIME};
$largeFileTimeInCache = checkVar($largeFileTimeInCache);


if ($debug) {
   print "jardir:   $skvjar\n";
}


my $cmdOut;  # reusable var for command output
my $lsCmd;  # reusable var for directory listing output

#-- Find path root
if ($debug) {
   print "--> ------------------------- \n\n";
   print "\n--> current directory is $currentDir\n";
   print "--> ------------------------- \n\n";
   print "--> tempspace dir is $tempspaceDir\n";
   print "--> ------------------------- \n\n";
}

chdir $currentDir;  #--necessary for cron

#-- Get file count in tempspace
if ($debug) {
   $lsCmd = "ls -l $tempspaceDir/*|wc";
   print "--> command is $lsCmd \n";
   $cmdOut = `$lsCmd`;
   print "--> file count:  $cmdOut";
   print "--> ------------------------- \n\n";
}

#--- Look for statistics file
my $stats = "$statDir/$statFile";
if ($debug) {
   print "--> stat file is $stats\n"; 
   print "--> -------------------------\n\n";
}
if (! -e $stats) {
   print "*** Error: stats file $stats was not found\n\n";
   exit();
}

if ($debug) {
   my $pwdCmd = "pwd";
   $cmdOut = `$pwdCmd`;
   print "--> pwd $cmdOut";
   print "--> ------------------------- \n\n";
}

#-- Check if jar files are present
if (! -e "$skvjar" ) {
   print "*** Error: jar file   does not exist \n\n";
   exit;
}

#-- Create command
my $javaPath = "-cp $skvjar";
my $className = "skyview.ops.DirScan";
my $classArgs = "/web_chroot/cache/results/fits time=$timeInCache largetime=$largeFileTimeInCache large=$largeFileSize debug=true recordFile=\"$statDir/$statFile\" email=\"laura.m.mcdonald\@nasa.gov\" mailCmd=\"./mailSender.pl\"";

#-- Run command
my $javaCmd = "/data/bin/java ". $javaPath ." " . $className . " " . 
   $classArgs; 

if ($debug) {
   print "--> command is $javaCmd\n";
}
$cmdOut = `$javaCmd`;
##-- uncomment for java command output which includes stats for all files
#if ($debug) {
   #print "\n--> java command output: $cmdOut \n";
   #print "--> ------------------------- \n\n";
#}

#-- Get file count update in tempspace
if ($debug) {
   $lsCmd = "ls -l $tempspaceDir/*|wc";
   print "--> command is $lsCmd \n";
}
if ($debug) {
   $cmdOut = `$lsCmd`;
   print "--> file count:  $cmdOut";
   print "--> ------------------------- \n\n";
   print "--> done.  \n\n";
}

sub checkVar {
   my ($input)= @_;
   if ($input =~ /([A-Za-z0-9.\/_]*)/) {
         $input= $1;  # to untaint 
   } else {
      print "Input error\n";
      exit();
   }
   return $input
}
